<?php
/**
 * fmmrt.php
 * Outil d'analyse et de suppression de code malveillant sur un site PHP
 *
 * @author    Folcomedia <contact@folcomedia.fr>
 * @link      https://www.folcomedia.fr
 * @copyright 2015 Folcomedia
 */

/**
 * Classe FMMRT (FolcoMedia Malware Removal Toolkit)
 * version 1.0.0
 *
 * Boîte à outils permettant d'identifier des codes malveillants sur un projet PHP pour
 * les supprimer de façon interactive. Cet outil est particulièrement adapté aux solutions
 * Open Source telles que Joomla, Wordpress, Drupal, Prestashop, souvent la cible d'attaques automatisées.
 *
 * Pour toute question ou suggestion, n'hésitez pas à nous contacter à l'adresse
 * e-mail : contact@folcomedia.fr
 */
class FMMRT
{

    /**
     * Constructeur
     *
     * @return FMMRT
     */
    public function __construct()
    {

        $this->startTime = microtime(true);
        $this->logFile = __DIR__.'/fmmrt.log';

        // Signatures à repérer
        $this->signatures = array(
            '[^>:]eval\s*\(',
            '[^>:]GLOBALS\s*\(',
            '[^>:](shell_|curl_)?exec\s*\('
        );

        // Hash fiables (basés sur le fichier, la ligne et le code)
        $this->whiteList = array(
            // Joomla 3.4
            '74d8ba124fa9f3f1c9230d39dac1f90d04817aea4c8ce770682b74be7907a3f5',
            'e58a5200aa6ed955af6fd43df0c4af2d747b10ea2c463cf7b98d70928b3e8782',
            'fefef0f79bd24f35fc688268f1f4b5189472bea061df3d88b0b42dc196a66851',
            'd3a00a500bb527e7940557354c47dbcd49f0677eed1ad8c2650b98b0333a100d',
            'baa6937c80d30d74fef165d851a7ca6c4b743730a8f1f26304aec327e45105ee',
            'd6290b8cb587de397252e33b1f10dabf62e30b1f1dcd3fbe59e211d2c85cea3f',
            '482796ad7678c4a5a12029cb2fe18b422a534d653ba880b913c349f35c84478a',
            '2a177ae6aac9924e6b9423c3e074ecbbda05fbb2e1341b98368008149d95579e',
            '7eed3e6417939d4bb7a98af3de10d4970f4af4709c1070f30ed1bca305aa557d',
            '0147a859c9ece29723dfdd248cbda33c8d8b647b01491be69782ddfcb23c2c4b',
            '94a437ac5816d9c656174813f41218e9411bd7aab86435dbc2d1207004292733',
            '7dc0a5acae73149889787954d8d3e01021447edbfc296b13e11e696c405ab882',
            '957802a561e04579156033fd9b70695de5b20add5e9d4d2ccfdea041e2f51028',
            '156375ec0c079093e5695e817ba1d12db66ddf632d55606a31848723e527846c',
            'de359d4d2bcc46900f0bde077e74db05c24341e1608860a3865690a9e1ba1ce8',
            // Piwik 2.1.11
            'a64839bba9bf97a3eec38f6aaece57ad608e983e8273880ae3524dcea27868d7',
            '8ed713c6cac6c884f2b176522758dd4098f5d99118c73ea4818565634d4a380d',
            '068a769c463869d1453b42550a49ede7bc9e8e667d156292b29da051806d3c50',
            '29985d8f39dee6d1006546ca40ed8bc88dc0797d369372c8b929cc19af91fff7',
            '936994b1c0d5c329c3ae79d5013351404b030220d7007b7b63deb302611a9fba',
            '82fa1e62ce379009c2c3a25444659e26ba2c5b14efe339592ea376629395f4d6',
            '33d16448a49a4357ec5b653e2e57a0473b8a6ecb8eb742a56dfb7c08028c6738',
            '758f03a7424807e840b2f50127637d50a0bb1bf2c828a6f46bc5618a158c992c',
            '08f513202d359e15508eaa206476609d1283ae99f1ad405a27e2cceb2d2459cf',
            '147803f13f8fec8d372c19f5dab0f9b4641164126686be6737b6cbc2fc151c45',
            '95606eb48e345008e7e15d9fae88d795cd25ce015135b85f9545c85130c75f88',
            'd8a6371f9a0c65420af48068f4bb8b5e1ea99896208f307855f01b4db1f638d6',
            '6ca895ea0a20898c83411b509bb1e604e3683b083bf3e70cb05f598406d4f13e',
            'ab9b36a86e92515646cd18781e39af4bc14cb4ddf023a0f0cccc171c02f7556e',
            '2462fb37ad5666dc542c77ea5756c45e118ce67eb170f5366c3c8de3c6863116',
            'ec21ce2b31e06e83068160a96e172b701c5dfaac06ca897ee47b755f6021007a',
            '2755da0709ca335ed45c8102fe0644025f43b1c89691582442044917343c6d54',
            'f045ea3e0e81c3559e3f3181a071bfbf0b0d6854e77f8017b9c63c10df276771',
            '1dfea628b1d9db20846e64da3f7dc4921c0d71b14c855f8eecc156547262b3d3',
            'e05cf1679fae306670edbe32ea71f5f41b101220d771ec58bbe49755db6bbef2',
            'f0737b8f849e1e175102895625fac491f7e49680f38a6b6d29ae90e4a7827171',
            'ab9a68d4090295f852fab0b95dbd63c596d3a1e47aab56522c2e03602170a7bf',
            'a0654aa25a76cc8a1dd8c08c367282c385bb25f33718801019d11683f36dbe09',
            '40dabd2a67b75e91f1265e64e7281999511146e71d98daae997c3900890b21bb',
            '001769eb755534a26a1488ba14f140098f8d95f3130235c7944c066dfcc3671a',
            '3441598022d874ea98b3d6a345c8bc9dec9ea24729a2efc708652ffb0489633c',
            'd3934dc0cfd47af8dea4227e74825b96516e3de35a0dca479d568fadd78e798e',
            'a6d2247bf6c6b3322f1aaf0f6b48314998ee24ce3dea4774a216227643c5a1c4',
            'ecae2af33993f337d7b6daf28d879b437e51e33981562df6fed2a7adf8ba26d5',
            'cc6973cafdf974220e9619d6383a9ca595f54ed13a598a111c6ea6220981b6ff',
            '36daa8e2978be4fa8c22eda2a93d91e14b1e96d6788a3d3ed0ed8f02d2783149',
            '483ba7b7ba238cce37330a7497b4e85a76b78cca46f4426f2cbf267e575d5acc',
            '5302faf894ecce273440208de67ab8ec3e296ddcecbba38db20133995b5e99e8',
            '20c5044d4121e399db7d9938f19ac816aa2542593ab69208e1a80ed7e5b4c97e',
            '92f986e5f4100e0a2184a030de9d58ba84922dae0eaf2bb39c0129ac9dec2b3e',
            '0765f302e7b2bd80bc80a43273914675dfde9cdebbbb08a9998b0762f6b7ee14',
            '19c9949fe29ea94027a5b049ad5bd7fe52adc1dcaf19a1f7464c2410ff769a3a',
            'dd9c01927b86816bd7abf2f8b0753f72cdad50aed0317802db8781a642e174c9',
            'ef645f310ba47113c1879378128d38a33bc05a517a7e186060c28517950f5596',
            '6889a9d4cee6236d451bd76448fc1ab740d2e3214e4f9c3d7295427a0a72a2ce',
            '82bb8cee3e08b4bff11d2409648a88c51e4a72c0a455240145d0056c14ecc96f',
            '7ddb0cc5cec864c81592c52b125858cbd1c5b6ba99db734e7bf39a41c9d9cfa1',
            'fe92417e8268ef8817c37ae03a57231da959557107eefa7a287473f67f1f8b06',
            '04b72055fdd6725ab8e1a5e69e0e89065297c661ab6147e1cbc392001e423ef2',
            '7686e399f9ec32d95e11830d798715f8c790723bdd36fcaf85edb724c3635ae1',
            '38e186609e37130f2341724480bd9402caf4fc095adf08907fc5f21e8c99fa15',
            'f1a743cd97cdb526c1a2cf8b1060aed2219220d1988df3b5b8a256939a58773d',
            '4511dc60b6bd5ed5fe7e8fdc92dbcedb4c53fc3acfd9b67c98ead0208ffc711f',
            '599095065bcff5d6b977e07b3ea1286dd155f788a0d10bae6c497fa35f470657',
            'b35dd09c3795f48942ca312fc3070278415afbed0cfd47e4a11901c7b1229295',
            '2691bf8c724728842e580653f8771b4f511e23529d45ea10a289302ab54d4dc8',
            '9f832bb0da54761f66916abe3f90f99cfbb4fd640b1e0457de3ae7206b240ef1',
            '197c858ff7322c67f6cb90baa3a5fd9e8b49865cfc20d3b9c23e8d16a29b592b',
            '8dc3195abd63563c39adefb9bd160c24526092e669183e57c8a0404678f13976',
            '6859050f43fa4d4ae853198a4165c8e58808ed11bbf603f328acda896dfd0e41',
            '2cd43e8de1243bca85115a53fb268ca17ae527f7b0290a365375ed2e18a3c607',
            '6b5e61911762ff0c4f069e9bb8816ab4d29119dbf5eeed82b2d556e1501d6549',
            '06f6db90c2477c1e59408bb981e3c56b258a265c1868577cdc8a7131cfc06537',
            '2c77e90acf0e23b4d84a730075427d0d3abf11341b012b4ad23150713152710f',
            '383bdb4ac931468e3d3eb6d1c6956d797358cb68dfec7029a690515efc2b0738',
            '8f6b3b0cea05377a4d2456a91ac032a8e7036ff7392f12f66141be2b0d349f2e',
            '678c1b581f76921d299811d44cbf293fc93110705a0eb7044e2ed5fbfc31adf0',
            '481576aba00495c80c67267e9cc3b601019c3fad1afecb9baea490c6887a7b4d',
            '7487b553d0891233b572df677f0fbeb1ff339bebd18344cc0746fd64125210d3',
            '6bd5add3cd7ca65832e7998ac15188a907154c967d56fb6b686a3892694cbc48',
            '2b8f161a472f12b8174417f624d27efd663aa356a46313d5e95cfaf3784ec37c',
            '2862e1b03b1aa67482b934d04790e7c62ae0e02dca87a5ea5efa5ea714223ffd',
            'ffa00fddfd47c63f3d5ca0b02d4f82ddaff7f249cb8f1d772be0d34f47256c1a',
            '32ac2752b9477f24926f94039b9841edc56f2700711a499cd1623d64a12aa741',
            'ece8bf026b031246f590a6f8a3d16991691882fb083eaf3850fb08ec4a3c0ab9',
            'b2afa27df8f237f91529af8e9e038259ebc3fe9aeffae10c60a1788877e2ccbf',
            'e552eee7cc9ff3aecdd2a491971ae1139bcaf05d1eb97c29dce94497adeb0f21',
            '95fba529eb1b2be8b07814780791b8483030ea4e329d1128c956471e95db4dac',
            'f137989d4ae3cf526bd2e431522fbbb3fd520164572dbe1fe8d54aca0088fa2b',
            'a2d1219554cb5f0d19d5c01e92c8afd11f32a833d238e80c0ea1706db21a0b48',
            '4bea00efb656479c43e7dfcb353d263b4f5fa81b3281c3ba6b86c8c0bb527392',
            'a6c09bdc2a948f8678ba65175628a566ec2cf70cfd03aa306e7942c9b6ff4122',
            '2527601d1e0e5028c6fbf2ca3c6855c51351128a90b24007fed39701f1b7b056',
            '4d1475ca9ed6bdf3cfcd1d7de9c924ebf7b5de8be005ee4b3af81a24c5e7d05a',
            'b765fb5fb4b36fc5b368f11ba90ee63308f856e23716c11e70c25cf286e4ae68',
            '5ef22ed9cf4240e2520fb1973748978422d08df6502a764f6f92544d1b3e9dfe',
            '8d3054dc56241e8f4e462b42a935e3119f7a82a1e06bd310bfc727a4c3f6a157',
            '7100560b7d5faace9dec565eb3b79118a249d69ba9992413aff7ed5ba973f423',
            'efd5e73866bf8ea1c30965293f796595a8656bd9293739885b368aa9c34966fd',
            '5bda4a7a15abdf324a7967deb848de0ae6622fb854094796173742cc50a801a4',
            'ffaa80abb44546722f4a9db2c3e087db92c44708d2746252516069c18af77443',
            '4f631dacba131d24d1ef29fe9aa679d86bab97ab4d464f97fb67075629b198a6',
            '5f8faaf5e80761ef19369a16ecb0e1a3c7171ab768ab1fb23dd12e1c2acc2cbb',
            '80cee201d3a8057393c1616fcc87ccd1d2f104f9d3a72968184988d4033cfce3',
            '0607804636123f808b72147e457dee4729b864382cd203162e71b24693945eda',
            '35960e6cc6993b6450ec390b5f45b20f2be78e1717e2159d4e883439e3a3dbe4',
            '2cba2d1ae7023d34fb0e427b7c8aafba73a5f87f0fc565ad25642e3c9ba5af7c',
            '8b7ac75b061f0a933a933b8f0575dbc888a8d85c286948aab1c3a2f4781cffec',
            'e81d01649cd167232651c6b8cadec996afe7046ce0c6a24a0b1aac04978fa66b',
            '9311c2c8198ab8f633732ba824790f46e61300bdeecfc1e16cd98a916d9caf27',
            '89f4998c90379293550e7f6b6a107d661884a3fa8465a003af6ac546e9bf243e',
        );

    }

    /**
     * Prise en charge l'action demandée (controller)
     *
     * @param string $action - Action à réaliser
     *
     * @return void
     */
    public function handleAction($action)
    {

        switch ($action) {

            // Analyse d'un répertoire
            // Remplit un fichier de log pour être exploité de façon interactive
            case 'analyse' :
                // Répertoire à analyser
                $this->requestDir = (isset($_REQUEST['dir']) ? trim($_REQUEST['dir']) : '');
                $dir = __DIR__.'/'.$this->requestDir;
                $dir = rtrim($dir, '/');
                // Fichiers déjà analysés (en cache)
                $this->parsedFiles = $this->getLogData()->files;
                // Récupère la liste des fichiers à analyser
                $this->filesToParse = array();
                $this->getFilesToParse($dir);
                $files = array_diff($this->filesToParse, $this->parsedFiles);
                // Analyse les fichiers
                foreach ($files as $file) {
                    $this->analyseFile($file);
                }
                break;

            // Récupère la liste des contenus à risque
            // Fichier, ligne, extrait de contenu
            case 'updateList' :
                $data = $this->getLogData();
                echo json_encode(array_diff_key($data->codes, array_flip($data->ignore)));
                exit();

            // Ajouter un fichier à la liste des fichiers ignorés
            // Actif tant que l'on ne vide pas le cache
            case 'ignore' :
                $hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : '';
                $data = $this->getLogData();
                if (!empty($hash) && !in_array($hash, $data->ignore)) {
                    array_push($data->ignore, $hash);
                    $this->writeLogData($data);
                }
                exit();

            case 'delCache' :
                unlink($this->logFile);
                exit();

            // Voir le contenu d'un fichier
            case 'see' :
                $hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : '';
                $data = $this->getLogData();
                if (array_key_exists($hash, $data->codes)) {
                    $code = $data->codes[$hash];
                    $content = htmlspecialchars(file_get_contents(__DIR__.'/'.$code->file));
                    $content = str_replace(htmlspecialchars($code->extrait), '<span style="color:red">'.htmlspecialchars($code->extrait).'</span>', $content);
                    echo '<div class="modal-header">';
                        echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                        echo '<h4 class="modal-title">'.$code->file.'</h4>';
                    echo '</div>';
                    echo '<div class="modal-body">';
                    echo '<pre>'.nl2br($content).'</pre>';
                    echo '</div>';
                    echo '<script type="text/javascript">';
                    echo "var scrollOffset = ".(($code->line - 10) * 18.15).";";
                    echo '</script>';
                }
                exit();

        }

    }

    /**
     * Analyse d'un répertoire
     *
     * @param string $dir - Répertoire à analyser
     *
     * @return void
     */
    public function getFilesToParse($dir)
    {

        // Dépassement temps d'exécution
        $this->checkAnalyseTime();

        // Liste tous les fichiers
        $h = opendir($dir);
        while ($h && ($f = readdir($h)) !== FALSE) {
            $path = $dir.'/'.$f;
            if (is_dir($path)) {
                if ($f != '.' && $f != '..') {
                    $this->getFilesToParse($path);
                }
            } else {
                $ext = strtolower(array_pop(explode('.', $f)));
                // TODO : conserver seulement php / inc ?
                if (in_array($ext, array('php', 'inc'))) {
                    array_push($this->filesToParse, $path);
                }
            }
        }


    }

    public function analyseFile($file)
    {

        // Dépassement temps d'exécution
        $this->checkAnalyseTime();

        // Lecture du fichier
        $content = file_get_contents($file);

        // On recherche chaque signature
        foreach ($this->signatures as $sign) {
            preg_match_all('/'.$sign.'/i', $content, $matches);
            // Pour chaque signature trouvée on extrait le code
            $startPos = 0;
            foreach ($matches[0] as $m) {
                // Détection ligne
                $startPos = strpos($content, $m, $startPos+1);
                $line = sizeof(explode(PHP_EOL, substr($content, 0, $startPos)));
                // On capture le code jusqu'à la paranthèse fermante correspondante
                $endPos = $startPos + strlen($m);
                $openBr = 1;
                while ($openBr > 0) {
                    $char = substr($content, $endPos, 1);
                    if ($char == '(') $openBr++;
                    if ($char == ')') $openBr--;
                    $endPos++;
                    // Prendre en charge ce cas de figure : paranthèse(s) non fermée(s)
                    if ($endPos > strlen($content)) {
                        break;
                    }
                }
                if ($endPos - $startPos > strlen($m) + 1) {
                    // Stockage du code potentiellement malveillant
                    $badCode = substr($content, $startPos+1, $endPos - $startPos - 1);
                    $relativePath = str_replace(__DIR__.'/', '', $file);
                    $hash = hash('sha256', $relativePath.$line.$badCode);
                    if (!in_array($hash, $this->whiteList)) {
                        $this->addFoundCode((object) array(
                            'file' => $relativePath,
                            'line' => $line,
                            'extrait' => $badCode,
                            'hash' => $hash
                        ));
                    }
                }
            }
        }

        // On enregistre ce fichier comme lu
        $this->addParsedFile($file);

    }

    /**
     * Récupère les données enregistrées dans le fichier de log
     *
     * @return stdClass - Données enregistrées
     */
    private function getLogData()
    {

        // TODO : flock() ?
        // On essaie de récupérer les données existantes

        $content = file_get_contents($this->logFile);

        // Construction du fichier de base
        if (empty($content)) {
            $data = new stdClass();
        } else {
            $data = unserialize($content);
        }

        // Champs prévus
        if (!isset($data->codes)) {
            $data->codes = array();
        }
        if (!isset($data->files)) {
            $data->files = array();
        }
        if (!isset($data->ignore)) {
            $data->ignore = array();
        }

        return $data;

    }

    /**
     * Met à jour le fichier de log
     *
     * @param stdClass $data - Données enregistrées
     *
     * @return void
     */
    private function writeLogData($data)
    {

        // TODO : flock() ?
        file_put_contents($this->logFile, serialize($data));

    }

    /**
     * Ajoute les détails d'un potentiel code malveillant
     * au fichier de log
     *
     * @param stdClass $code - Détails du code (fichier, ligne, extrait)
     *
     * @return void
     */
    private function addFoundCode($code)
    {

        // On récupère les données stockées
        $data = $this->getLogData();

        // On ajoute le code seulement s'il n'y est pas encore
        if (!array_key_exists($code->hash, $data->codes)) {
            $data->codes[$code->hash] = $code;
            $this->writeLogData($data);
        }

    }

    /**
     * Ajoute un fichier à la liste des fichiers déjà traités
     *
     */
    private function addParsedFile($file)
    {

        // On récupère les données stockées
        $data = $this->getLogData();

        // On ajoute le code seulement s'il n'y est pas encore
        if (!in_array($file, $data->files)) {
            array_push($data->files, $file);
            $this->writeLogData($data);
        }

    }

    /**
     * Vérifie la durée de traitement pour ne pas avoir un timeout error
     *
     * @return void
     */
    private function checkAnalyseTime()
    {

        if (microtime(true) - $this->startTime > 10) {
            header('Location: '.$_SERVER['PHP_SELF'].'?task=analyse&dir='.$this->requestDir);
            exit();
        }

    }

}

// --- Prise en charge de l'action demandée ---
$fmmrt = new FMMRT();
$fmmrt->handleAction(isset($_REQUEST['task']) ? $_REQUEST['task'] : '');

// --- Définition des dictionnaires ---
$lang = 'fr';
$dico =  array(
    'fr' => array(
        'DISCLAIMER' => "Disclaimer : Boîte à outils développée par l'agence web <strong>Folcomedia</strong> permettant d'identifier des codes malveillants sur un projet PHP pour les supprimer de façon interactive. Cet outil est particulièrement adapté aux solutions Open Source telles que Joomla, Wordpress, Drupal, Prestashop, souvent la cible d'attaques automatisées. Pour toute question ou suggestion, n'hésitez pas à nous contacter à l'adresse contact@folcomedia.fr et à visiter notre site internet <a href='https://www.folcomedia.fr' target='_blank'>https://www.folcomedia.fr</a>.",
        'PAGE_TITLE' => "Détection et suppression de code malveillant - Folcomedia Malware Removal Toolkit",
        'ANALYSER' => "Lancer l'analyse",
        'DELETE_CACHE' => "Supprimer le cache",
        'REMOVE_TOOL' => "Supprimer cet outil",
        'REP_A_ANALYSER' => "Répertoire à analyser",
        'HOME_TITLE' => "Bienvenue dans Folcomedia Malware Removal Toolkit&nbsp;!",
        'HOME_MSG' => "Si vous avez placé le fichier à la racine de votre site internet, vous pouvez dès à présent cliquer sur le bouton &laquo;&nbsp;Lancer l'analyse&nbsp;&raquo; ci-dessus pour débuter.",
        'HOME_MSG2' => "Vous pouvez également indiquer un répertoire (par exemple &laquo;&nbsp;administrator&nbsp;&raquo;) dans lequel vous voulez concentrer l'analyse, pour cela indiquez le dans le champ situé à gauche du bouton.<br />Tous les sous-répertoires seront également analysés.",
        'LOADING_RESULT' => "Analyse en cours, les résultats vont apparaitre ci-dessous...",
        'IGNORE_BTN' => "Ignorer ce code",
        'SEE_IN_CONTEXT_BTN' => "Voir dans le contexte du fichier",
        'REMOVE_BTN' => "Supprimer l'extrait de code",
        'ANALYSER_APRES_CLEAR_CACHE' => "Le cache a été supprimé. Voulez-vous lancer une analyse ?"
    )
);
foreach ($dico[$lang] as $key => $value) {
    define($key, $value);
}

?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><?php echo PAGE_TITLE ?></title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        #Header { background:#def; font-size:12px; padding:10px; }
        #Actions { background:#cde; padding:10px; }
        #Content { padding-top:20px }
        .box { padding:40px 0; border-bottom:1px dashed #ddd; }
        .buttons .btn-primary { margin-left:10px; }
        .modal-body pre { max-height:400px; overflow:auto; white-space:nowrap; }
    </style>
</head>
<body>
    <div id="Actions">
        <div class="container form-inline">
            <div class="input-group">
                <div class="input-group-addon">/</div>
                <input id="AnalyseDir" type="text" class="form-control" placeholder="<?php echo REP_A_ANALYSER ?>" />
            </div>
            <button id="LaunchTest" type="button" class="btn btn-success"><?php echo ANALYSER ?></button>
            <button id="DeleteCache" type="button" class="btn btn-default"><?php echo DELETE_CACHE ?></button>
            <button id="DeleteTool" type="button" class="btn btn-danger pull-right"><?php echo REMOVE_TOOL ?></button>
        </div>
    </div>
    <div id="Main">
        <div id="Content" class="container">
            <div class="jumbotron">
                <h1><?php echo HOME_TITLE ?></h1>
                <br />
                <p><?php echo HOME_MSG ?></p>
                <p><?php echo HOME_MSG2 ?></p>
                <hr />
                <p><?php echo DISCLAIMER ?></p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            // Lancement de l'analyse
            $('#LaunchTest').click(function(e) {
                // Activation / Désactivation du bouton
                // Lancement de la requete AJAX
                var btn = e.currentTarget;
                if (!$(btn).hasClass('disabled')) {
                    $(btn).addClass('disabled');
                    $.ajax({
                        url: '<?php echo $_SERVER['PHP_SELF'] ?>?task=analyse&dir='+$('#AnalyseDir').val(),
                        success: function() {
                            $(btn).removeClass('disabled');
                        }
                    });
                }
                // Prise en charge UI
                $('#Content').html('<p class="text-center"><?php echo addslashes(LOADING_RESULT) ?></p><div id="List"></div>');
                // MAJ Régulières
                    updateList();
            });

            // Suppression du cache
            $('#DeleteCache').click(function(e) {
                $.ajax({
                    url: '<?php echo $_SERVER['PHP_SELF'] ?>?task=delCache',
                    success: function() {
                        if (confirm('<?php echo addslashes(ANALYSER_APRES_CLEAR_CACHE) ?>')) {
                            $('#LaunchTest').trigger('click');
                        }
                    }
                });
            });

            // Ignore fichier
            $(document).on('click', 'button.action-ignore', function(e) {
                var box = $(e.currentTarget).parents('.box');
                var hash = $(box).data('hash');
                $.ajax({
                    url: '<?php echo $_SERVER['PHP_SELF'] ?>?task=ignore&hash='+hash,
                    success: function(xhr) {
                        $(box).fadeOut({
                            complete: function() {
                                $(box).remove();
                            }
                        });
                     }
                });
            });

            // Voir fichier
            $(document).on('click', 'button.action-see', function(e) {
                var box = $(e.currentTarget).parents('.box');
                var hash = $(box).data('hash');
                var modal = $('<div class="modal"></div>').html('<div class="modal-dialog modal-lg"><div class="modal-content"></div></div>').appendTo($(document.body)).modal('show');
                $(modal).on('hidden.bs.modal', function (e) {
                    $(modal).remove();
                });
                $.ajax({
                    url: '<?php echo $_SERVER['PHP_SELF'] ?>?task=see&hash='+hash,
                    success: function(xhr) {
                        $(modal).find('.modal-content').html(xhr);
                        $('.modal-body pre').animate({scrollTop: scrollOffset});
                    }
                });
            });

        });

        // MAJ Liste
        function updateList()
        {

            $.ajax({
                 url: '<?php echo $_SERVER['PHP_SELF'] ?>?task=updateList',
                 success: function(xhr) {
                     var codes = $.parseJSON(xhr);
                     $.each(codes, function(hash, code) {
                         addBox(code);
                     });
                 }
             });

             if ($('#LaunchTest').hasClass('disabled')) {
                 setTimeout(updateList, 500);
             } else {
                 $('#Content').find('p.text-center').remove();
             }

        }

        // Ajoute une boite d'information par code sensible découvert
        function addBox(code)
        {

            // Utile pour lister les signatures
            /*if (!($('div[data-hash="'+code.hash+'"]').length)) {
                $('<div class="box" data-hash="'+code.hash+'">').html("'"+code.hash+"',").appendTo($('#List'));
            }
            return;*/


            if (!($('div[data-hash="'+code.hash+'"]').length)) {
                // On ajoute la boite seulement si elle n'est pas encore présente
                var box = $('<div class="box" data-hash="'+code.hash+'">').appendTo($('#List'));
                $('<div>').html('<strong>'+code.file+' #'+code.line+'</strong>').appendTo(box);
                $('<pre>').html(code.extrait).appendTo(box);
                var buttons = $('<div>').addClass('buttons').appendTo(box);
                $('<button>').addClass('btn btn-default action-ignore').html('<?php echo addslashes(IGNORE_BTN) ?>').appendTo(buttons);
                $('<button>').addClass('btn btn-primary action-see').html('<?php echo addslashes(SEE_IN_CONTEXT_BTN) ?>').appendTo(buttons);
                <?php /* $('<button>').addClass('btn btn-danger pull-right action-remove').html('<?php echo addslashes(REMOVE_BTN) ?>').appendTo(buttons); */ ?>
            }

        }

    </script>
</body>
</html>