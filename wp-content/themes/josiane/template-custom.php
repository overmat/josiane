<?php
/**
 * Template Name: Custom Template
 */
?>
<?php
$pages = get_pages(array(
    'sort_column' => 'menu_order',
    'sort_order' => 'ASC'
));
$i = 0;
foreach ($pages as $key => $page):
    $isInSlide = get_field('in_slide', $page->ID);
    $imageTitle = get_field('image_title', $page->ID);
    $pauseSlide = get_field('pause_slide', $page->ID);
    if($isInSlide):
        ?>
        <div data-key="<?php echo $i; ?>" data-title="<?php echo $page->post_title; ?>" id="page-title-<?php echo $i; ?>" class="page-title <?php if($i == 0){echo 'current first-page-title';} ?>" data-pause="<?php echo $pauseSlide; ?>" data-text="text-<?php echo $i; ?>" data-img="<?php echo $imageTitle; ?>"></div>
        <?php
        $i++;
    endif;
endforeach;
$i = 0;
foreach ($pages as $key => $page):
    $isInSlide = get_field('in_slide', $page->ID);
    if($isInSlide):
        $content = apply_filters( 'the_content', $page->post_content );
        $content = str_replace( ']]>', ']]&gt;', $content );
        ?>
        <div id="text-<?php echo $i; ?>" class="page-text">
          <?php if($page->post_name == 'brainmade'): ?>
            <?php get_template_part('templates/content-brainmade'); ?>
          <?php else: ?>
            <?php echo $content; ?>
            <?php endif; ?>
        </div>
        <?php
        $i++;
    endif;
endforeach;
?>
<div class="main-wrapper margin-50">
    <div class="josiane-title-wrapper">
        <div id="image-page-wrapper" class="image-page-wrapper">
        </div>
        <div class="image-site-wrapper">
            <img id="josiane-title" class="josiane-title" src="<?php echo get_template_directory_uri();?>/assets/images/josiane-home-title.png" />
        </div>
    </div>
    <div id="current-text-page" class="text-page">
    </div>
</div>
