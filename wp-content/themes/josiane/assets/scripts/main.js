/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function () {
                // JavaScript to be fired on all pages
            },
            finalize: function () {

                $('.margin-50').each(function () {
                    $(this).css('margin-top', $(window).height() / 2 - 37.5 + 'px');
                });

                $( window ).resize(function() {
                    $('.margin-50').each(function () {
                        $(this).css('margin-top', $(window).height() / 2 - 37.5 + 'px');
                    });
                });

                $('#current-text-page').hide();

                var elements = [];

                $('.page-title').each(function () {
                    var element = {
                        image: $(this).attr('data-img'),
                        text: $("#page-text-" + $(this).attr('data-key')).attr('data-text')
                    };

                    elements.push(element);
                });

                $('.other .wp-titles-main-title .wp-tiles-byline-title').each(function() {

                    $(this).css('margin-top', ($(this).parent().height() / 2) - ($(this).height() / 2));
                });

                $('.other .wp-tiles-byline .wp-tiles-byline-wrapper').each(function() {

                    $(this).css('margin-top', ($(this).parent().height() / 2) - ($(this).height() / 2));
                });

                function moveTitle() {

                    var page = $('.current');
                    var attrImg = page.attr('data-img');

                    if (typeof attrImg !== typeof undefined && attrImg !== false) {

                        var img = $('<img>');
                        img.attr('src', $('.current').attr('data-img'));
                        img.appendTo('#image-page-wrapper');
                        var image = new Image();
                        image.src = img.attr('src');
                        image.onload = function () {
                            var width = this.width;

                            $('#image-page-wrapper').animate({
                                'width': width + 'px'
                            }, 800);
                        };

                        if (page.attr('data-pause') !== '1') {
                            img.hide().fadeIn(2200).delay(2600).fadeOut(2200);
                        } else {
                            img.hide().fadeIn(2200);
                        }
                    }

                    var attrText = page.attr('data-text');
                    if (typeof attrText !== typeof undefined && attrText !== false) {
                        $('#current-text-page').html($('#' + $('.current').attr('data-text')).html());
                    }

                    if(page.attr('data-pause') !== '1') {
                        $('#current-text-page').hide().delay(1000).fadeIn(600).delay(3000).fadeOut(2200);
                    } else {
                        $('#current-text-page').hide().delay(1000).fadeIn(600);
                    }

                    if(page.attr('data-pause') !== '1') {
                        page.removeClass('current');
                        next = page.next('.page-title');

                        if (next.length > 0) {
                            page.next('.page-title').addClass('current');
                        } else {
                            $('.first-page-title').addClass('current');
                        }

                        t = setTimeout(function () {
                            moveTitle();
                        }, 7000);
                    }
                }

                moveTitle();


              $(document).on( 'click', '.filter-items button', function() {
                var filterValue = $(this).attr('data-filter');

                $('.filter-items button.active').removeClass('active');
                $(this).addClass('active');
                var i = 0;
                $('.post-wrapper').each(function () {
                  if($(this).hasClass(filterValue)) {
                    $(this).fadeIn();
                  }  else {
                    $(this).fadeOut();
                  }
                });

              });

              $('.wp-tiles-tile-bg').css({
                'background-image': "url('" + $('.wp-tiles-tile-bg').attr('data-main') + "')",

                /* Internet Explorer: */
                '-ms-filter': "progid:DXImageTransform.Microsoft.AlphaImageLoader( src='" + $('.wp-tiles-tile-bg').attr('data-main') + "', sizingMethod='scale')", /* IE8 */
                'filter': "progid:DXImageTransform.Microsoft.AlphaImageLoader( src='" + $('.wp-tiles-tile-bg').attr('data-main') + "', sizingMethod='scale')" /* IE6 & IE7 */
              });

              $('.layer-wrapper').each(function() {

                $(this).css('margin-top', ($(this).parent().height() / 2) - ($(this).height() / 2));
              });

              $('.main-title').each(function() {

                $(this).css('margin-top', ($(this).parent().height() / 2) - ($(this).height() / 2));
              });

            }
        },
        // Home page
        'home': {
            init: function () {
                // JavaScript to be fired on the home page
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },

        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
