<header class="banner">
    <nav class="navbar navbar-main">

            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav clearfix']);
            endif;
            ?>

    </nav>
</header>