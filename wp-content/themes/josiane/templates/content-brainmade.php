<?php
  $cat = get_category_by_slug('brainmade');
  $children = get_categories(array('parent' => $cat->cat_ID));
?>
  <div class="filter-items">
    <?php foreach ($children as $child): ?>
      <button <?php if ($child->category_nicename == 'most-viewed'): ?>class="active"<?php endif; ?> data-filter="<?php echo $child->category_nicename; ?>"><?php echo $child->name; ?></button>
    <?php endforeach; ?>
  </div>
<?php
  $posts = get_posts(array(
    'category' => 'brainmade',
    'numberposts' => -1
  ));
  
  $el = 0;
  $actives = array();
  foreach ($posts as $post):
    
    $tile_classes = array();
    $categories = get_the_category( $post->ID );
    foreach( $categories as $category ) {
      $tile_classes[] = $category->slug;
      
      if($category->category_nicename == 'most-viewed') {
        $actives[$post->ID] = true;
      }
    }
    
    $el++;
    $first = true;
    $dataOthers = '';
    for($i = 2;$i<6;$i++) {
      $im = get_field('miniature_'.$i, $post->ID);
      
      if($im != false && $im != '' ) {
        if(!$first) {
          $dataOthers .= '|';
        } else {
          $first = false;
        }
        $dataOthers .= $im;
      }
    }

    ?>
    <a href="<?php echo get_permalink($post->ID); ?>">
      <article class="post-wrapper <?php echo implode(' ', $tile_classes); ?>" itemscope itemtype="http://schema.org/CreativeWork" style="<?php if(!isset($actives[$post->ID])): ?>display:none;<?php endif; ?>background-image: url('<?php echo get_field('miniature', $post->ID); ?>');" data-main="<?php echo get_field('miniature', $post->ID); ?>" data-others="<?php print $dataOthers; ?>" data-duration="<?php print get_field('slide_interval',$post->ID); ?>">
        <div class="main-title">
          <h2 itemprop="name" class="post-main-title"><?php echo apply_filters( 'the_title', $post->post_title, $post->ID ) ?></h2>
        </div>
        <div class='layer'>
          <div class="layer-wrapper">
            <div class="main-title-wrapper">
              <h2 itemprop="name" class="post-main-title"><?php echo apply_filters( 'the_title', $post->post_title, $post->ID ) ?></h2>
            </div>
            <div class='caption' itemprop="description">
              <?php echo get_field('caption_miniature', $post->ID); ?>
            </div>
          </div>
        </div>
      </article>
    </a>
    <?php
    if($el==3) {
      $el = 0;
    }
  endforeach;

?>
