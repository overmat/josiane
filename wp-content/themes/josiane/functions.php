<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'posts_to_posts',
        'from' => 'post',
        'to' => 'post',
        'cardinality' => 'many-to-many',
        'admin_box' => array(
            'show' => 'from',
            'context' => 'side'
        ),
        'title' => array(
            'from' => 'Article "read more"',
        ),
        'sortable' => 'from'

    ) );
}
add_action( 'p2p_init', 'my_connection_types' );