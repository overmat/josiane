<?php
  /**
   * Template Name: Brainmade Template
   */
?>

<div class="current" data-pause="1"></div>
<?php
  $post = the_post();
  $imageTitle = get_field('image_title', $post->id);
?>
<div class="main-wrapper margin-50">
  <div id="line-title" class="line"></div>
  <div class="josiane-title-wrapper">
    <div id="image-page-wrapper" class="image-page-wrapper title-h1">
      <img src="<?php echo $imageTitle; ?>" />
    </div>
    <div class="image-site-wrapper">
      <img id="josiane-title" class="josiane-title" src="<?php echo get_template_directory_uri();?>/assets/images/josiane-home-title.png" />
    </div>
  </div>
  <div id="current-text-page-l" class="text-page wp-tiles-grid wp-tiles-byline-align-top wp-tiles-byline-animated wp-tiles-byline-fade-in wp-tiles-loaded">
    <?php get_template_part('templates/content-brainmade'); ?>
  </div>
</div>

