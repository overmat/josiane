<?php
$post = get_post();
$imageTitle = get_field('image_title',$post->ID);
?>

        <?php while (have_posts()) : the_post(); ?>
            <div class="josiane-title-wrapper">
                <img src="<?php echo $imageTitle; ?>" /><img id="josiane-title" class="josiane-title" src="<?php echo get_template_directory_uri();?>/assets/images/josiane-home-title.png" />
            </div>
          <?php get_template_part('templates/content', 'page'); ?>
        <?php endwhile; ?>
