/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        $('.content-slide.vertical-align').each(function() {
          $(this).css('margin-top', ($(window).height() / 2) - ($(this).height() / 2));
        });
      },
      finalize: function() {
        var swiper = new Swiper('.swiper-container', {
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        });

        swiper.on('transitionEnd', function () {
          $('.swiper-wrapper').removeClass('height-100');

          if($('.swiper-slide-active .text-page').is(':offscreen')) {
            $('.swiper-container').addClass('overflow-y');
          } else {
            $('.swiper-container').removeClass('overflow-y');
          }

          $('.swiper-wrapper').addClass('height-100');
        });


          $('#slider').slideReveal({
              trigger: $("#menu-link"),
              position: "left",
              push: false,
              overlay: true,
              show: function(slider, trigger){
                  $('#slider').show();
              },
              hidden: function(slider, trigger){
                $('#slider').hide();
              }
          });
      }


    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  jQuery.expr.filters.offscreen = function(el) {
    var rect = el.getBoundingClientRect();
    return (rect.y + rect.height > window.innerHeight);
  };

})(jQuery); // Fully reference jQuery after this point.

$(document).ready(function() {
  $('.wp-titles-main-title .wp-tiles-byline-title').each(function() {

    $(this).css('margin-top', ($(this).parent().height() / 2) - ($(this).height() / 2));
  });

  $('.wp-tiles-byline .wp-tiles-byline-wrapper').each(function() {

    $(this).css('margin-top', ($(this).parent().height() / 2) - ($(this).height() / 2));
  });
});
