<?php
/**
 * Template Name: Custom Template
 */
?>
<div class="swiper-container">
    <a href="#nav-panel" id="menu-link"><i class="fa fa-bars" aria-hidden="true"></i></a>
    <div class="swiper-wrapper height-100">

            <?php
            $pages = get_pages(array(
                'sort_column' => 'menu_order',
                'sort_order' => 'ASC'
            ));
            $i = 0;

            foreach ($pages as $key => $page):
                $isInSlide = get_field('in_slide', $page->ID);
                $imageTitle = get_field('image_title', $page->ID);
                $pauseSlide = get_field('pause_slide', $page->ID);
                $content = apply_filters( 'the_content', $page->post_content );
                $content = str_replace( ']]>', ']]&gt;', $content );
                $inMobile = get_field('on_mobile', $page->ID);
                $isVerticalAlign = get_field('vertical_align', $page->ID);

                if($inMobile):
                    ?>
                    <div class="swiper-slide">
                        <div class="content-slide <?php if($isVerticalAlign): ?>vertical-align<?php endif; ?>">
                            <div class="josiane-title-wrapper">
                                <img src="<?php echo $imageTitle; ?>" /><img id="josiane-title" class="josiane-title" src="<?php echo get_template_directory_uri();?>/assets/images/josiane-home-title.png" />
                            </div>
                            <div class="text-page">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                endif;
            endforeach;
            ?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
</div>
