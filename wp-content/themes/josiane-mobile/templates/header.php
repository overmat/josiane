<div id='slider'>
    <div data-role="panel" data-display="overlay" data-theme="b" id="nav-panel">
        <a href="<?php echo get_home_url(); ?>">
            <img id="josiane-title" class="josiane-title" src="<?php echo get_template_directory_uri();?>/assets/images/josiane-home-title.png" />
        </a>
        <?php
        if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
    </div>
</div>