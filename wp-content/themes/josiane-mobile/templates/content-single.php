<?php
//construction des liens de partage sur les réseaux sociaux
//https://www.facebook.com/dialog/feed?app_id={app_id}&link={link}&picture={picture}&name={name}&caption={caption}&description={description}&redirect_uri={redirect_uri}

//Facebook
//numéro unique d’application Facebook
$app_id = "1463351637280317" ;
//$app_id = "155351561227154";
//$app_id = "448679238607137";
//URL de la page en cours
$link = urlencode(get_permalink()) ;
//URL d’une image
//print_r(get_field('visuel_reseaux_sociaux'));
//$picture = get_field('visuel_reseaux_sociaux')
$picture = urlencode(get_field('visuel_reseaux_sociaux')) ;
//nom de la page
$name = urlencode(get_field('titre_partage')) ;
//sous-titre de la page
$caption = urlencode(get_field('sous-titre_partage')) ;
//description de la page
$description = urlencode(get_field('description_partage')) ;
//URL pour revenir vers votre page, celle de la page en cours par exemple
$redirect_uri = urlencode("http://www.josianefaitdelapub.com/") ;

$lien_facebook = "https://www.facebook.com/dialog/feed?app_id=".$app_id."&link=".$link."&picture=".$picture."&name=".$name."&caption=".$caption."&description=".$description."&redirect_uri=".$redirect_uri;
//$lien_facebook = "";

//Twitter
//URL à partager
$url = urlencode(get_permalink()) ;
//message par défaut du tweet
$text = urlencode(get_field('titre_partage')) ;
//compte Twitter pour ajouter un « via @sunfox » par exemple à la fin (optionnel)
$via = urlencode("FollowJosiane") ;

$lien_twitter = "http://twitter.com/intent/tweet/?url=".$url."&text=".$text."&via=".$via;
//$lien_twitter = "";

//Google +
//URL à partager
$url = urlencode(get_permalink());
//Code langue pour la page de partage (optionnel)
$hl = urlencode("fr") ;

$lien_google = "https://plus.google.com/share?url=".$url."&hl=".$hl;
//$lien_google = "";

$encoded_url = urlencode( getUrl() );
function getUrl() {
  $url  = isset( $_SERVER['HTTPS'] ) && 'on' === $_SERVER['HTTPS'] ? 'https' : 'http';
  $url .= '://' . $_SERVER['SERVER_NAME'];
  $url .= in_array( $_SERVER['SERVER_PORT'], array('80', '443') ) ? '' : ':' . $_SERVER['SERVER_PORT'];
  $url .= $_SERVER['REQUEST_URI'];
  return $url;
}
?>

<div class="current" data-pause="1"></div>
<div class="main-wrapper margin-50">
  <div id="line-title" class="line"></div>

      <div class="josiane-title-wrapper tile-post">
        <?php
        $category = wp_get_post_categories($post->ID);
        $imageTitle = get_field('image_post_title', $post->ID);
        ?>
        <div id="image-page-wrapper" class="image-page-wrapper title-h1">


          <div id="image-page-wrapper" class="image-page-wrapper title-h1 title-post">
            <h1><?php the_title(); ?></h1>
          </div>


        </div>
      </div>


  <?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
      <div class="entry-content">
        <?php the_content(); ?>
      </div>
    </article>
  <?php endwhile; ?>

</div>
<div class="post-footer">
  <div class="title-share">
    <img id="josiane-share" class="josiane-share" src="<?php echo get_template_directory_uri();?>/assets/images/sharejosiane-<?php echo $category[0]; ?>.png" />
  </div>
  <div class="network-list clearfix">
    <a href="<?php echo $lien_twitter; ?>" target="_blank"><img class="icon icon-twitter" src="<?php echo get_template_directory_uri();?>/assets/images/icon-twitter.png" /></a>
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $encoded_url; ?>"><img class="icon icon-fb" src="<?php echo get_template_directory_uri();?>/assets/images/icon-fb.png" /></a>
    <a href="<?php echo $lien_google; ?>" target="_blank"><img class="icon icon-gplus" src="<?php echo get_template_directory_uri();?>/assets/images/icon-gplus.png" /></a>
  </div>
  <?php
  $previous = get_previous_post(true);
  $previousMiniature = get_field('miniature', $previous->ID);
  $next = get_next_post(true);
  $nextMiniature = get_field('miniature', $next->ID);

  $connected = new WP_Query( array(
      'connected_type' => 'posts_to_posts',
      'connected_items' => get_queried_object(),
  ) );
  if ( $connected->have_posts() ) :
    ?>
    <div class="other clearfix">

      <?php $i=0;while ( $connected->have_posts() ) : $connected->the_post();
        $post = get_post();
        $thumb = get_field('miniature', get_post()->ID);
        $caption = get_field('caption_miniature', get_post()->ID);

        $categories = wp_get_post_categories($post->ID);
        $displayTitle = false;
        foreach ($categories as $categoryId) {
          if($categoryId == 2) {
            $displayTitle = true;
          }
        }

        ?>

        <a href="<?php the_permalink(); ?>">
          <div class="<?php if($i == 1) { echo 'middle ';} ?>previous-post" style="background: url(<?php echo $thumb; ?>) no-repeat center;background-size: cover;">
            <?php if($displayTitle): ?>
              <div class="wp-titles-main-title"><h2 itemprop="name" class="wp-tiles-byline-title"><?php echo apply_filters( 'the_title',  $post->post_title,  get_post()->ID ) ?></h2></div>
            <?php endif; ?>
            <div class="titles">
              <div class="titles-wrapper">
                <h2 class="title"><?php the_title(); ?></h2>
                <?php if($caption !== null && $caption !== false && $caption !== ''): ?>
                  <div class="subtitle"><?php echo $caption; ?></div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </a>
        <?php $i++;endwhile; ?>
    </div>
    <?php
    wp_reset_postdata();
  endif;

  $back[2] = array(
      'link' => get_permalink(47),
      'text' => 'Back to josiane\'s work'
  );
  $back[3] = array(
      'link' => get_permalink(1526),
      'text' => 'Back to josiane\'s blog'
  );
  $back[38] = array(
      'link' => get_permalink(49),
      'text' => 'Back to josiane\'s news'
  );
  ?>
  <div class="back-link">
    <a href="<?php echo $back[$category[0]]['link']; ?>">
      <?php echo $back[$category[0]]['text']; ?>
    </a>
  </div>
</div>


