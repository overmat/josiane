<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <div data-role="page" id="page-<?php the_ID(); ?>" class="main-page" data-dom-cache="true" data-prev="<?php echo get_permalink(get_previous_post()); ?>" data-next="<?php echo get_permalink(get_next_post()); ?>" data-url="<?php the_permalink(); ?>">
        <?php
          do_action('get_header');
          get_template_part('templates/header');
        ?>
        <div role="main" class="ui-content">
            <?php if(!is_front_page()): ?>
                <button id="menu-link"><i class="fa fa-bars" aria-hidden="true"></i></button>
            <?php endif; ?>
            <?php include Wrapper\template_path(); ?>
        </div>
    </div>
    <?php
      wp_footer();
    ?>

  </body>
</html>
