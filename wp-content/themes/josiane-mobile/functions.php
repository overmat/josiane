<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


add_shortcode('wp-tiles', 'wptiles');
function wptiles($param) {
    $category = get_category_by_slug($param['category']);

    $posts = get_posts(array(
        'numberposts' => -1,
        'category' => $category->term_id
    ));

    $return = '';
    foreach ($posts as $post) {
        $img = get_field('miniature', $post->ID);

        $categories = wp_get_post_categories($post->ID);
        $displayTitle = false;
        foreach ($categories as $categoryId) {
            if($categoryId == 2) {
                $displayTitle = true;
            }
        }

        $return .= '
        <a href="'.get_permalink($post->ID).'">
        <article class="post-list">
            <img class="thumb" src="'.$img.'" />';

        if($displayTitle) {
            $return .= '<div class="wp-titles-main-title"><h2 itemprop="name" class="wp-tiles-byline-title">'.apply_filters( 'the_title', $post->post_title, $post->ID ).'</h2></div>';
        }

        $return .= '<div class="wp-tiles-byline">
                <div class="wp-tiles-byline-wrapper">
                    <h2 itemprop="name" class="wp-tiles-byline-title" style="color: rgb(0, 59, 94);">'.apply_filters( 'the_title', $post->post_title, $post->ID ).'</h2>
                    <div class="wp-tiles-byline-content" itemprop="description">'.get_field('caption_miniature', $post->ID).'</div>
                </div>
            </div>
        </article></a>';

    }

    return $return;
}

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'posts_to_posts',
        'from' => 'post',
        'to' => 'post',
        'cardinality' => 'many-to-many',
        'admin_box' => array(
            'show' => 'from',
            'context' => 'side'
        ),
        'title' => array(
            'from' => 'Article "read more"',
        ),
        'sortable' => 'from'

    ) );
}
add_action( 'p2p_init', 'my_connection_types' );