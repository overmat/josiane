jQuery(document).ready(function($) {

	// Redirection mobile selon taille d'écran
    if ($(window).width() < 767 && document.location.host != 'm.josianefaitdelapub.com' && document.location.search != '?show=popup') {
        document.location.href = 'http://m.josianefaitdelapub.com'+document.location.pathname;
    }

});
