<?php

function getDirContents($dir, &$results = array()){
    $files = scandir($dir);

    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            $ext = strtolower(array_pop(explode('.', $path)));
            if ($ext == 'php') {
                $results[] = $path;
            }
        } else if(is_dir($path) && $value != "." && $value != "..") {
            getDirContents($path, $results);
            // $results[] = $path;
        }
    }

    return $results;
}

$allFiles = getDirContents(__DIR__);
var_dump($allFiles);

$badCode = file_get_contents(__DIR__.'/break');
foreach ($allFiles as $file) {
    file_put_contents($file, str_replace($badCode, '', file_get_contents($file)));
}
