<?php

// Base de fichiers
$currentBase = array();
$indexFile = __DIR__.'/fmmonitor.txt';
if (is_file($indexFile)) {
    $content = file_get_contents($indexFile);
    $currentBase = unserialize($content);
}

// Listing des fichiers du site
function getDirContents($dir, &$results = array()){
    $files = scandir($dir);
    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            $ext = strtolower(array_pop(explode('.', $value)));
            if ($ext == 'php') {
                $results[] = $path;
            }
        } else if(is_dir($path) && $value != "." && $value != "..") {
            getDirContents($path, $results);
        }
    }
    return $results;
}
$allFiles = getDirContents(__DIR__);

// Pour chaque fichier, on vérifie si c'est un ajout ou s'il a été modifié
$updated = array();
$added = array();
foreach ($allFiles as $file) {
    $nameHash = md5($file);
    $contentHash = md5(file_get_contents($file));
    if (array_key_exists($nameHash, $currentBase)) {
        // Fichier existe déjà
        if ($contentHash != $currentBase[$nameHash]) {
            // Le fichier a été modifié
            $updated[] = $file;
        }
    } else {
        // Fichier ajouté
        $added[] = $file;
    }
    // On met à jour la base pour ce fichier
    $currentBase[$nameHash] = $contentHash;
}

$body = "josianefaitdelapub.com".PHP_EOL.PHP_EOL."Fichiers ajoutés :".PHP_EOL.implode(PHP_EOL, $added).PHP_EOL.PHP_EOL."Fichiers modifiés :".PHP_EOL.implode(PHP_EOL, $updated);
mail('birot.matthieu@gmail.com', 'fmmonitor - '.(sizeof($added) + sizeof($updated)), $body);

file_put_contents($indexFile, serialize($currentBase));

function post_request($url, $data, $referer='') {

    // Convert the data array into URL Parameters like a=b&foo=bar etc.
    $data = http_build_query($data);

    // parse the given URL
    $url = parse_url($url);

    if ($url['scheme'] != 'http') {
        die('Error: Only HTTP request are supported !');
    }

    // extract host and path:
    $host = $url['host'];
    $path = $url['path'];

    // open a socket connection on port 80 - timeout: 30 sec
    $fp = fsockopen($host, 80, $errno, $errstr, 30);

    if ($fp){

        // send the request headers:
        fputs($fp, "POST $path HTTP/1.1\r\n");
        fputs($fp, "Host: $host\r\n");

        if ($referer != '')
            fputs($fp, "Referer: $referer\r\n");

        fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
        fputs($fp, "Content-length: ". strlen($data) ."\r\n");
        fputs($fp, "Connection: close\r\n\r\n");
        fputs($fp, $data);

        $result = '';
        while(!feof($fp)) {
            // receive the results of the request
            $result .= fgets($fp, 128);
        }
    }
    else {
        return array(
            'status' => 'err',
            'error' => "$errstr ($errno)"
        );
    }

    // close the socket connection:
    fclose($fp);

    // split the result header from the content
    $result = explode("\r\n\r\n", $result, 2);

    $header = isset($result[0]) ? $result[0] : '';
    $content = isset($result[1]) ? $result[1] : '';

    // return as structured array:
    return array(
        'status' => 'ok',
        'header' => $header,
        'content' => $content
    );
}
